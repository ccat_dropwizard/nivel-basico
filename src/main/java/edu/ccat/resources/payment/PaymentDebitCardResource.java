package edu.ccat.resources.payment;


import com.google.inject.Inject;
import edu.ccat.modules.annotations.DebitCard;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("payments/debitcard")
public class PaymentDebitCardResource {

    private final PaymentService service;

    @Inject
    public PaymentDebitCardResource(@DebitCard PaymentService service) {
        this.service = service;
    }

    @GET
    public Response pay() {
        this.service.pay();
        return Response.ok("Payment DONE").build();
    }
}
