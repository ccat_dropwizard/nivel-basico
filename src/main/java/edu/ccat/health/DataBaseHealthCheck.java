package edu.ccat.health;

import com.codahale.metrics.health.HealthCheck;

public class DataBaseHealthCheck extends HealthCheck {

    private boolean up;

    public DataBaseHealthCheck(boolean up) {
        this.up = up;
    }

    @Override
    protected Result check() throws Exception {
        return up
            ? Result.healthy()
            : Result.unhealthy("The DB is down because Wilson");
    }
}
