package edu.ccat.modules;

import edu.ccat.DwRestConfiguration;
import edu.ccat.config.CursoInfo;
import edu.ccat.modules.annotations.DebitCard;
import edu.ccat.modules.annotations.TigoMoney;
import edu.ccat.resources.payment.PaymentService;
import edu.ccat.resources.payment.PaymentServiceDebitCard;
import edu.ccat.resources.payment.PaymentServiceTigoMoney;
import edu.ccat.resources.student.StudentDao;
import edu.ccat.resources.student.StudentDaoStub;
import edu.ccat.resources.student.StudentH2Dao;
import edu.ccat.resources.student.StudentService;
import edu.ccat.resources.student.StudentServiceImpl;
import edu.ccat.resources.teacher.TeacherDao;
import edu.ccat.resources.teacher.TeacherDaoImpl;
import edu.ccat.resources.teacher.TeacherService;
import edu.ccat.resources.teacher.TeacherServiceImpl;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class MainModule extends DropwizardAwareModule<DwRestConfiguration> {

    @Override
    protected void configure() {
        bind(CursoInfo.class).toInstance(this.configuration().cursoInfo);
        bind(StudentService.class).to(StudentServiceImpl.class);
        //bind(StudentDao.class).to(StudentDaoStub.class);
        bind(StudentDao.class).to(StudentH2Dao.class);
        bind(TeacherService.class).to(TeacherServiceImpl.class);
        bind(TeacherDao.class).to(TeacherDaoImpl.class);

        bind(PaymentService.class).annotatedWith(TigoMoney.class).to(PaymentServiceTigoMoney.class);
        bind(PaymentService.class).annotatedWith(DebitCard.class).to(PaymentServiceDebitCard.class);
        /*
        bind(TeacherDao.class).annotatedWith(High.class).to(TeacherDaoHigh.class)
        bind(TeacherDao.class).annotatedWith(Mid.class).to(TeacherDaoMid.class)
        */
    }
}
