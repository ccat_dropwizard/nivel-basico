package edu.ccat.exceptions;

public enum DwRestExceptionType {

    ID_NOT_FOUND,
    FEW_ELEMENTS,
    UNKNOWN
}
