package edu.ccat.core;

import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloManaged implements Managed {

    final static Logger LOGGER = LoggerFactory.getLogger(HelloManaged.class);
    @Override
    public void start() throws Exception {
        LOGGER.debug("====> Starting the HelloManaged instance");
    }

    @Override
    public void stop() throws Exception {
        LOGGER.debug("====> Stopping the HelloManaged instance");
    }
}
