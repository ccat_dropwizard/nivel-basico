package edu.ccat;

import edu.ccat.config.CursoInfo;
import edu.ccat.config.SchoolInfo;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class DwRestConfiguration extends Configuration {
    // POJO: Plain Old Java Object

    @NotEmpty
    public String schoolName;

    @JsonProperty("int")
    public String intValue = "5";

    public SchoolInfo schoolInfo;

    public CursoInfo cursoInfo;

    public List<String> arreglo;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    public DataSourceFactory getDatabase() {
        return this.database;
    }

    public void setDatabase(DataSourceFactory dataSourceFactory) {
        this.database = dataSourceFactory;
    }
}
