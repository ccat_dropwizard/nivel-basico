package edu.ccat.resources.teacher;

import com.google.inject.Inject;
import edu.ccat.api.Teacher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TeacherServiceImpl implements TeacherService {

@Inject
private  TeacherDao dao;


    @Override
    public List<Teacher> getAll() {
        return dao.getAll();
    }
}
