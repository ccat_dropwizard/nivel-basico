package edu.ccat.config;

public class SchoolInfo {

    public String name;
    public String address;
    public String url;

    @Override
    public String toString() {
        return "SchoolInfo{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
