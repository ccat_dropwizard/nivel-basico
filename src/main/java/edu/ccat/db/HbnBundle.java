package edu.ccat.db;

import edu.ccat.DwRestConfiguration;
import edu.ccat.api.Student;
import edu.ccat.api.Teacher;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;

public class HbnBundle extends HibernateBundle<DwRestConfiguration> {

    public HbnBundle() {
        super(Student.class);
    }

    @Override
    public PooledDataSourceFactory getDataSourceFactory(DwRestConfiguration configuration) {
        return configuration.getDatabase();
    }
}
