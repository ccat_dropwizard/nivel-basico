package edu.ccat.resources.student;

import com.google.inject.Inject;
import edu.ccat.api.Student;
import edu.ccat.exceptions.DwRestException;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StudentResource {

    private final StudentService service;

    @Inject
    public StudentResource(StudentService service) {
        this.service = service;
    }

    @GET
    @UnitOfWork
    public List<Student> getAllStudents() throws DwRestException {
        return this.service.getAll();
    }

    @POST
    @UnitOfWork
    public Response registerStudent(@Valid Student student) {
        long createdId = this.service.create(student);

        return Response.created(URI.create("students/" + Long.toString(createdId)))
                .build();
    }

    @GET
    @Path("{id}")
    @UnitOfWork
    public Response getStudent(@PathParam("id") LongParam id) throws DwRestException {
        return Response.status(404)
                .entity(this.service.getById(id.get()))
                .build();
    }

    /*
    @GET
    public List<Integer> getAllStudentsScores() {
        var students = this.dao.getAll();
        students.stream(); // logic here
        return students;
    }*/
}
