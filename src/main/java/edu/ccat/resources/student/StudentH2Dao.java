package edu.ccat.resources.student;

import com.google.inject.Inject;
import edu.ccat.api.Student;
import edu.ccat.exceptions.DwRestException;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class StudentH2Dao extends AbstractDAO<Student> implements StudentDao {

    @Inject
    public StudentH2Dao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Student> getAll() throws DwRestException {
        return list(currentSession().createQuery("from Student"));
    }

    @Override
    public long create(Student student) {
        return persist(student).getId();
    }

    @Override
    public Student getById(long id) throws DwRestException {
        return get(id);
    }
}

