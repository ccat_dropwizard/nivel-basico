package edu.ccat.exceptions;

public class DwRestException extends Throwable {

    private int code;
    private DwRestExceptionType type;

    public DwRestException() {
        this(500, DwRestExceptionType.UNKNOWN);
    }

    public DwRestException(DwRestExceptionType type) {
        this.code = 200;
        this.type = type;
    }

    public DwRestException(int code, DwRestExceptionType type) {
        this.code = code;
        this.type = type;
    }

    public DwRestException(int code, DwRestExceptionType type, String message) {
        super(message);
        this.code = code;
        this.type = type;
    }

    public int getCode() {
        return this.code;
    }

    public DwRestExceptionType getType() {
        return this.type;
    }
}

