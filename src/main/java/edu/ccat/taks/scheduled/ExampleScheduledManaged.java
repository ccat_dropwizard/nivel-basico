package edu.ccat.taks.scheduled;

import io.dropwizard.lifecycle.Managed;

import java.util.concurrent.atomic.AtomicInteger;

public class ExampleScheduledManaged implements Managed {

    private ExampleScheduledService service = new ExampleScheduledService();

    @Override
    public void start() throws Exception {
        this.service.startAsync().awaitRunning();
    }

    @Override
    public void stop() throws Exception {
        this.service.stopAsync().awaitTerminated();
    }
}
