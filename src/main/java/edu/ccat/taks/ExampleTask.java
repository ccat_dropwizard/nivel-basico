package edu.ccat.taks;

import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

public class ExampleTask extends Task {

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    public ExampleTask() {
        super("example-task");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        logger.info("Example task executed");
    }
}
