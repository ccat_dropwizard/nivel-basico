package edu.ccat.resources.payment;


import com.google.inject.Inject;
import edu.ccat.modules.annotations.TigoMoney;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("payments/tigomoney")
public class PaymentTigoMoneyResource {

    private final PaymentService service;

    @Inject
    public PaymentTigoMoneyResource(@TigoMoney PaymentService service) {
        this.service = service;
    }

    @GET
    public Response pay() {
        this.service.pay();
        return Response.ok("Payment DONE").build();
    }
}
