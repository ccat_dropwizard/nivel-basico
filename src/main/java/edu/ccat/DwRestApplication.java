package edu.ccat;

import edu.ccat.core.HelloCommand;
import edu.ccat.core.HelloManaged;
import edu.ccat.db.HbnBundle;
import edu.ccat.health.DataBaseHealthCheck;
import edu.ccat.modules.HbnModule;
import edu.ccat.modules.MainModule;
import edu.ccat.resources.HelloResource;
import edu.ccat.resources.InfoResource;
import edu.ccat.resources.student.StudentResource;
import edu.ccat.resources.student.StudentServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;

public class DwRestApplication extends Application<DwRestConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DwRestApplication().run(args);
    }

    @Override
    public String getName() {
        return "DwRest";
    }

    @Override
    public void initialize(final Bootstrap<DwRestConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/ui", "/ui", "index.html", "ui"));
        bootstrap.addBundle(new AssetsBundle("/assets/ui/css", "/css", "styles.css", "css"));
        bootstrap.addBundle(new AssetsBundle("/assets/admin", "/admin", "admin.html", "admin"));

        final HbnBundle hibernate = new HbnBundle();
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(GuiceBundle.builder()
                .enableAutoConfig(getClass().getPackage().getName())
                .modules(new MainModule(), new HbnModule(hibernate))
                .searchCommands()
                .printDiagnosticInfo()
                .build()//"edu.ccat"
        );
    }

    @Override
    public void run(final DwRestConfiguration configuration,
                    final Environment environment) {
        /*
        HelloResource resource = new HelloResource();
        environment.jersey().register(resource);
        HelloResource resource2 = new HelloResource();
        InfoResource infoResource = new InfoResource(configuration.cursoInfo);
        environment.jersey().register(infoResource);

        environment.healthChecks().register("Database Down",
                new DataBaseHealthCheck(false));

        environment.healthChecks().register("Database Up",
                new DataBaseHealthCheck(true));

        environment.lifecycle().manage(new HelloManaged());

        StudentResource studentResource = new StudentResource(new StudentServiceImpl());
        environment.jersey().register(studentResource);*/

    }

}
