package edu.ccat.core;

import io.dropwizard.cli.Command;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

public class HelloCommand extends Command {

    public HelloCommand() {
        super("hello", "Say hello to someone");
    }

    @Override
    public void configure(Subparser subparser) {
        subparser.addArgument("-n", "--name")
                .dest("name")
                .type(String.class)
                .required(true)
                .help("This is the name of somebody");
    }

    @Override
    public void run(Bootstrap<?> bootstrap, Namespace namespace) throws Exception {
        System.out.println("Hello " + namespace.getString("name"));
    }
}
