package edu.ccat.resources;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

@Path("/hello")
public class HelloResource {

    @GET
    @Timed
    public String sayHello()  {
        return "Hello";
    }

    @GET
    @Path("/name/{second}")
    public String sayName(@PathParam("second") String second) {
        return "hello " + second;
    }

    @GET
    @Path("/second")
    public String saySecond(@QueryParam("second") String second) {
        return "Query Data " + second;
    }


}
