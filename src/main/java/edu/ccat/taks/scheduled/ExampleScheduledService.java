package edu.ccat.taks.scheduled;

import com.google.common.util.concurrent.AbstractScheduledService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ExampleScheduledService extends AbstractScheduledService{

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    private AtomicInteger counter = new AtomicInteger();

    @Override
    protected void runOneIteration() throws Exception {
        logger.info("Counter:" + counter.addAndGet(2));
        logger.info("-----------------> Scheduled task executed ");
    }

    @Override
    protected Scheduler scheduler() {
        return AbstractScheduledService.Scheduler
                .newFixedRateSchedule(5, 15, TimeUnit.SECONDS);
    }
}
