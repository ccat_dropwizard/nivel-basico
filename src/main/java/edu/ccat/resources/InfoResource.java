package edu.ccat.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import edu.ccat.DwRestConfiguration;
import edu.ccat.config.CursoInfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class InfoResource {

    @Inject
    private CursoInfo cursoInfo;

    @GET
    @Path("info")
    public CursoInfo getCursoInfo() {
        return cursoInfo;
    }





}
