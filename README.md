# Curso Dropwizard 3ra versión

Herramientas a instalar:
---

- [Maven](https://maven.apache.org)
- [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- IDE con soporte para Maven. Sugerido [IDEA](https://www.jetbrains.com/idea/download/)

Recursos:
---
- [Presentación GDrive](https://drive.google.com/open?id=1ZovHI2WBnXn0sOeXCLpOPLrimo_0SDtclwAIqocSCHc)
- [Manual Dropwizard](http://www.dropwizard.io/1.1.2/docs/manual/index.html)


# Como ejecutar la aplicación

Construir y ejecutar:
---

1. Ejecutar `mvn clean install` para construir.
2. Iniciar la aplicación con `java -jar target/dwrest-1.0-SNAPSHOT.jar server config.yml`
3. Verificar que se esta ejecutando en `http://localhost:8080`

Chequeos de salud
----

Revisar la salud de las aplicaciones en `http://localhost:8081/healthcheck`

