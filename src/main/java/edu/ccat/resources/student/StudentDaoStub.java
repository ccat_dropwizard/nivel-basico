package edu.ccat.resources.student;

import edu.ccat.api.Student;
import edu.ccat.exceptions.DwRestException;
import edu.ccat.exceptions.DwRestExceptionType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDaoStub implements StudentDao {

    private final List<Student> students = new ArrayList<>();

    @Override
    public List<Student> getAll() throws DwRestException {
        if (students.size() < 5) throw new DwRestException(DwRestExceptionType.FEW_ELEMENTS);
        return this.students;
    }

    @Override
    public long create(Student student) {
        if (this.students.add(student))
            return student.getId();
        //throw new NotCreatedInstanceException();
        return 0;
    }

    @Override
    public Student getById(long id) throws DwRestException {
        List<Student> students = this.students
                .stream()
                .filter(s -> s.getId() == id)
                .collect(Collectors.toList());

        if (students.isEmpty()) return null;
        return students.get(0);
    }
}
