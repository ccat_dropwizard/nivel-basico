package edu.ccat.resources.teacher;

import edu.ccat.api.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAll();
}
