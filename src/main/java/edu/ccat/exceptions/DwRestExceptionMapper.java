package edu.ccat.exceptions;

import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DwRestExceptionMapper implements ExceptionMapper<DwRestException> {
    @Override
    public Response toResponse(DwRestException exception) {
        switch (exception.getType()) {
            case UNKNOWN: return this.handleUnknown();
            case FEW_ELEMENTS: return this.handleFewElements();
            case ID_NOT_FOUND: return this.handleIdNotFound();
        }
        /*
        switch(exception.getEnumClass) {
            case a: Response.a;
            case a: Response.a;
        }*/
        return Response
                .status(exception.getCode())
                .entity(exception.getMessage())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private Response handleIdNotFound() {
        return Response
                .status(HttpStatus.NOT_FOUND_404)
                .entity("Resource not found")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private Response handleFewElements() {
        return Response
                .status(HttpStatus.EXPECTATION_FAILED_417)
                .entity("Few elements found")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private Response handleUnknown() {
        return Response.ok()
                .entity("All OK but with error")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
