package edu.ccat.resources.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentServiceDebitCard implements PaymentService {

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    @Override
    public void pay() {
        logger.info("Debit Card Processed");
    }
}
