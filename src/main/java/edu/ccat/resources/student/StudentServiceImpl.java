package edu.ccat.resources.student;

import com.google.inject.Inject;
import edu.ccat.api.Student;
import edu.ccat.exceptions.DwRestException;
import edu.ccat.exceptions.DwRestExceptionType;

import java.util.Collections;
import java.util.List;

public class StudentServiceImpl implements StudentService {

    private final StudentDao dao;

    @Inject
    public StudentServiceImpl(StudentDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Student> getAll() throws DwRestException {
        return this.dao.getAll();
    }

    @Override
    public long create(Student student) {
        return this.dao.create(student);
    }

    @Override
    public Student getById(long id) throws DwRestException {
        Student student = this.dao.getById(id);
        if (student == null) throw new DwRestException(404, DwRestExceptionType.ID_NOT_FOUND, "Student not exists");
        return student;
    }
}
