package edu.ccat.resources.teacher;

import com.google.inject.Inject;
import edu.ccat.api.Teacher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("teachers")
@Produces(MediaType.APPLICATION_JSON)
public class TeacherResource {

    private TeacherService service;

    @Inject
    public TeacherResource(TeacherService service) {
        this.service = service;
    }

    /*
    public TeacherResource(@High TeacherService service) {
        this.service = service;
    }

    public TeacherResource(@Mid TeacherService service) {
        this.service = service;
    }
    */

    @GET
    public List<Teacher> listAll() {
        return service.getAll();
    }




}
