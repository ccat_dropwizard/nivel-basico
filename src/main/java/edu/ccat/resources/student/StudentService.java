package edu.ccat.resources.student;

import edu.ccat.api.Student;
import edu.ccat.exceptions.DwRestException;

import java.util.List;

public interface StudentService {

    List<Student> getAll() throws DwRestException;

    long create(Student student);

    Student getById(long id) throws DwRestException;
}
